$(document).ready(function () {
    var start = true
    function startCircles () {
        if (start) {
            $('.circle1').animate({
                top: 0,
                left: 0,
                opacity: 1
            },500);
            $('.circle2').animate({
                top: 180,
                left: 130,
                opacity: 1
            },500);
            $('.circle3').animate({
                top: 270,
                left: 350,
                opacity: 1
            },500);
            $('.circle4').animate({
                top: 180,
                left: 570,
                opacity: 1
            },500);
            $('.circle5').animate({
                top: 0,
                left: 700,
                opacity: 1
            },500)
        }
        $('html, body').stop().animate({
            scrollTop: $('#startCircle .circle_big').offset().top
        }, 1000);
        start = false
    }
    $('#startCircle').on('mouseover', function() {
        $(this).removeClass('onload')
    }).on('click', function () {
        startCircles();
    });
    $('.circle').on('click', function () {
        $('#other').animate(
            {
                'marginTop': 100
            }
        );
        $('.circles').height(1350)
        var top = $(this).css('top');
        var left = $(this).css('left');
        if ($(this).hasClass('circleOpen') == false) {
            $(this).animate({
                top: 330,
                left: 350
            },500, function () {
                $(this).removeClass("circle1 circle2 circle4 circle5 animate").addClass('circle3')
                $(this).animate({
                        width: 700,
                        top: 330,
                        left: 100
                    },500, function () {
                    $(this).addClass('circleOpen').find('.text').fadeIn();
                    $('html, body').stop().animate({
                        scrollTop: $('.circle3').offset().top
                    }, 500);
                })
            });
        }
        if ($(this).hasClass('circle3') == false ) {
            $('.circle3').removeClass('circleOpen').find('.text').fadeOut('fast')
            $('.circle3').animate({
                top: top,
                left: left,
                width: 200
            },500, function () {
                $(this).removeClass('circle3').addClass('.animate')
            })
        }
    })
    $('#up').on('click', function () {
        $('html, body').stop().animate({
            scrollTop: 0
        }, 1000);
    })
    $('#menu_drop a[href^="#"]').click(function(){
        $('#menu_drop').removeClass('show-all').addClass('hide-all');
        $('#menu').removeClass('active')
        var target = $(this).attr('href');
        if (target == '#startCircle') {
            startCircles();
        } else {
            $('html, body').animate({scrollTop: $(target).offset().top-50}, 500);
            return false;
        }
    });
    /*var contactsLeft = 0;
    $('#contacts .next').on('click', function () {
        contactsLeft=-230;
        $('#contacts .stage2').css('left',contactsLeft);

    })
    $('#contacts .previous').on('click', function () {
        contactsLeft=0;
        $('#contacts .stage2').css('left',contactsLeft);
    })
    var partnersLeft = 0;
    $('#partners .next').on('click', function () {
        contactsLeft=-100;
        $('#partners .stage2').css('left',partnersLeft);

    })
    $('#partners .previous').on('click', function () {
        contactsLeft=0;
        $('#partners .stage2').css('left',partnersLeft);
    })*/
});