$(document).ready(function () {

    setTimeout(function(){
        $('#startCircle.animate').removeClass('onload').removeClass('animate').addClass('opened').unbind('click');
        $('.circle1').addClass('show').animate({ top: 0, left: 0},500);
        $('.circle2').addClass('show').animate({ top: 180, left: 130},500);
        $('.circle3').addClass('show').animate({ top: 270, left: 350},500);
        $('.circle4').addClass('show').animate({ top: 180, left: 570},500);
        $('.circle5').addClass('show').animate({ top: 0, left: 700},500);

        $('#content').css('height', '700px');
    }, 1000);

    var animation_progress = 0;
    $('.circle').bind('click', function() {
        if(animation_progress === 0 && !$(this).hasClass('circleOpen')) {
            $('#content').addClass('open');
            animation_progress = 2;
            var top = $(this).css('top');
            var left = $(this).css('left');

            $('.mid-section')
                .removeClass('mid-section')
                .removeClass('circleOpen')
                .addClass('animate')
                .animate({
                    top: top,
                    left: left,
                    width: 200
                }, 500, function() {
                    animation_progress -= 1;
                }).find('.text').fadeOut('fast');

            $(this)
                .addClass('mid-section')
                .removeClass('animate')
                .animate({
                    top: 330,
                    left: 350
                }, 500, function() {
                    setTimeout(function(){
                        $('.mid-section')
                            .animate({
                                width: 700,
                                top: 330,
                                left: 100
                            }, 500, function() {
                                if( !$(this).find('a.logo').length ) {
                                    $(this).addClass('circleOpen').find('.text').fadeIn('fast');
                                    $('html, body').stop().animate({
                                        scrollTop: $('.circleOpen').offset().top
                                    }, 1000);
                                }
                                animation_progress -= 1;
                            });
                    }, 300);
                });
        }

    });

    $('#up').on('click', function () {
        $('html, body').stop().animate({
            scrollTop: 0
        }, 1000);
    });

    if( $('#partners_carousel').length > 0 ) {
        $('#partners_carousel')
            .jcarousel({ wrap: 'circular' })
            .jcarouselAutoscroll({
                interval: 3000,
                target: '+=5',
                autostart: true
            });

        $('#partners_pagination .previous').jcarouselControl({ target: '-=1' });
        $('#partners_pagination .next').jcarouselControl({ target: '+=1' });
    }

    $.i18n.init({ pageDefaultLanguage: 'ru' }).done(function(){
        $('body').i18n();
    });

});